import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './@shared/layouts/app-layout/app-layout.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./@modules/homepage/homepage.module').then(m => m.HomepageModule)
      },
      {
        path: 'people',
        loadChildren: () => import('./@modules/character/character.module').then(m => m.CharacterModule)
      },
      {
        path: 'films',
        loadChildren: () => import('./@modules/film/film.module').then(m => m.FilmModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
