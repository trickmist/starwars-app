import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Adapter } from '../adapter';
import { ApiData } from '../api-data/api-data.model';
import { ApiError } from '../api-data/api-error.model';

export class ResourceService<T> {

  constructor(protected url: string, protected httpClient: HttpClient, protected adapter: Adapter<T>) {
    this.url = environment.apiUrl + url;
  }

  all(data?: {}): Observable<ApiData<T[]>> {
    const params = this.prepareParams(data);

    return this.httpClient.get<ApiData<T[]>>(this.url + '/', { params }).pipe(
      catchError((error) => {
        error = new ApiError(error.error);
        return throwError(error);
      }),
      map((response) => {
        response.results = this.adapter.adaptArray(response);
        return new ApiData<T[]>(response);
      }),
    );
  }

  get(id: string|null, data?: {}): Observable<T> {
    const params = this.prepareParams(data);

    return this.httpClient.get<T>(`${this.url}/${id}/`, { params }).pipe(
      catchError((response) => {
        const error = new ApiError(response.error);
        return throwError(error);
      }),
      map((response) => {
        return this.adapter.adapt(response);
      })
    );
  }

  save(data: any, id?: number): Observable<T> {
    const params = this.prepareParams(data);

    if (id === undefined) {
      return this.httpClient.post<T>(this.url, params).pipe(
        catchError((response) => {
          const error = new ApiError(response.error);
          return throwError(error);
        }),
        map((response) => {
          return this.adapter.adapt(response);
        })
      );
    } else {
      return this.httpClient.put<T>(`${this.url}/${id}`, params).pipe(
        catchError((response) => {
          const error = new ApiError(response.error);
          return throwError(error);
        }),
        map((response) => {
          return this.adapter.adapt(response);
        })
      );
    }
  }

  delete(id: number): Observable<T> {
    return this.httpClient.delete(`${this.url}/${id}`).pipe(
      catchError((response) => {
        const error = new ApiError(response.error);
        return throwError(error);
      }),
      map((response) => {
        return this.adapter.adapt(response);
      })
    );
  }

  prepareParams(data?: any): HttpParams {

    let params = new HttpParams();

    if (!data) {
      data = {};
    }

    const keys: string[] = Object.keys(data);

    if (!keys.length) {
      return params;
    }
    const values: string[] = Object.keys(data).map((key: string) => {
      return data[key];
    });

    keys.map((key, index) => {
      if (values[index] != null && values[index] !== '') {
        params = params.set(key, values[index]);
      }
    });

    return params;
  }

}
