import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { GlobalService } from '../services/global.service';
import { ResourceService } from './resource.service';

export class ResourceResolver<T> implements Resolve<T> {
  constructor(private globalService: GlobalService, private service: ResourceService<T>, private params?: any) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<T> | Promise<T> | any {
    return this.service.get(route.paramMap.get('id'), this.params).pipe(
      catchError(error => {
        this.globalService.is404.next(true);
        return of(error);
      }));
  }
}
