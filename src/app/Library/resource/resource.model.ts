export class Resource<T> {
  public slug?: string = '';

  public constructor(data?: {}) {
    Object.assign(this, data);
  }

}
