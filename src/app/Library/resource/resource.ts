export interface Resource<T> {
  init(): void;
}
