export class ApiData<T> {
  results?: T;
  next!: number;
  previous!: string;
  count?: number;

  public constructor(data?: {}) {
    Object.assign(this, data);
  }

}
