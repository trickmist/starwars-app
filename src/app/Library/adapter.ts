export interface Adapter<T> {
  adapt(item: any): T;
  adaptArray(items: any): T[];
}
