import { Injectable } from '@angular/core';
import { Adapter } from '../../../Library/adapter';
import { Resource as ResourceInterface } from '../../../Library/resource/resource';
import { Resource } from '../../../Library/resource/resource.model';
import { UserService } from '../../user/services/user.service';
import { take } from 'rxjs/operators';

export class Character extends Resource<Request> implements ResourceInterface<Character> {
  public id?: number;
  public name?: string;
  public birth_year?: string;
  public eye_color?: string;
  public gender?: string;
  public hair_color?: string;
  public height?: string;
  public mass?: string;
  public skin_color?: string;
  public homeworld?: string;
  public films?: string[];
  public species?: string;
  public starships?: string;
  public vehicles?: string;
  public url?: string;
  public created?: string;
  public edited?: string;
  public favorite?: boolean;

  public constructor(data?: {}) {
    super(data);
    this.init();
  }

  init(): void {
  }

  toggleFavorite(): void {
    this.favorite = !this.favorite;
  }

}

@Injectable({
  providedIn: "root"
})
export class CharacterAdapter implements Adapter<Character> {

  constructor(private userService: UserService) {
  }

  adapt(data: any): Character {
    const url = data.url.split('/');
    data.id = url[url.length - 2];
    data.favorite = this.userService.getFavorites().indexOf(data.id) >= 0;
    return new Character(data);
  }

  adaptArray(data: any): Character[] {
    data = data.results;
    return data.map((item: Character) => {
      return this.adapt(item);
    });
  }
}
