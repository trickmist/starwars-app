import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { take, takeUntil, tap } from 'rxjs/operators';
import { Character } from '../../models/character.model';
import { Subject } from 'rxjs';
import { CharacterService } from '../../services/character.service';
import { UserService } from '../../../user/services/user.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss']
})
export class CharacterDetailComponent implements OnInit, OnDestroy {

  ngUnsubscribe = new Subject();

  character: Character = new Character();
  homeworld?: any = undefined;
  films?: any[] = undefined;
  displayedColumns = ['film-episode-id', 'film-title', 'film-release-date'];


  constructor(
    private activatedRoute: ActivatedRoute,
    breakpointObserver: BreakpointObserver,
    private router: Router,
    private characterService: CharacterService,
    private userService: UserService
  ) {

    const layoutChanges = breakpointObserver.observe([
      '(max-width: 369.98px)',
      '(min-width: 370px) and (max-width: 599.98px)',
      Breakpoints.Small
    ]);
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.data.character) {
      this.character = this.activatedRoute.snapshot.data.character;
    }

    if (this.character) {
      this.characterService.getCharacterHomeworld(this.character).pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe(homeworld => {
        this.homeworld = homeworld;
      });

      this.characterService.getFilmsByCharacter(this.character).pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe((films => {
        this.films = films;
      }));
    }
  }

  toggleFavorite(): void {
    if (this.character.favorite) {
      this.userService.removeFromFavorites(this.character.id!);
    } else {
      this.userService.addToFavorites(this.character.id!);
    }
    this.character.toggleFavorite();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
