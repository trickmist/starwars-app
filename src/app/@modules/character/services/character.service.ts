import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResourceService } from '../../../Library/resource/resource.service';
import { Character, CharacterAdapter } from '../models/character.model';
import { combineLatest, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Film, FilmAdapter } from '../../film/models/film.model';


@Injectable({
  providedIn: "root"
})
export class CharacterService extends ResourceService<Character> {

  constructor(
    httpClient: HttpClient,
    adapter: CharacterAdapter,
    private filmAdapter: FilmAdapter
  ) {
    super('people', httpClient, adapter);
  }

  getCharacterHomeworld(character: Character): Observable<any> {
    return this.httpClient.get(character.homeworld!).pipe(
      take(1)
    )
  }

  getFilmsByCharacter(character: Character): Observable<Film[]> {
    const filmObservables: Observable<Film>[] = character.films!.map(
      film => this.httpClient.get<Film>(film)
    );
    return combineLatest(filmObservables).pipe(
      map((response) => {
        return this.filmAdapter.adaptArray({results: response});
      })
    );
  }

}
