import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/@shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { CharacterRoutingModule, ItemResolver } from './character-routing.module';
import { CharacterDetailComponent } from './components/character-detail/character-detail.component';
import { CharacterListComponent } from './components/character-list/character-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CharacterListComponent,
    CharacterDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    CharacterRoutingModule,
    SharedModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [
    ItemResolver
  ]
})
export class CharacterModule { }
