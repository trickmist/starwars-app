import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot } from '@angular/router';
import { ResourceResolver } from 'src/app/Library/resource/resource.resolver';
import { GlobalService } from 'src/app/Library/services/global.service';
import { CharacterModule } from './character.module';
import { CharacterDetailComponent } from './components/character-detail/character-detail.component';
import { CharacterListComponent } from './components/character-list/character-list.component';
import { Character } from './models/character.model';
import { CharacterService } from './services/character.service';
import { Observable, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ItemResolver<T> implements Resolve<T> {
  constructor(
    private globalService: GlobalService,
    private service: CharacterService,
    private httpClient: HttpClient
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Character> | Promise<Character> | any {
    return this.service.get(route.paramMap.get('id')).pipe(
      catchError(error => {
        this.globalService.is404.next(true);
        return of(error);
      }));
  }
}

const routes = [
  {
    path: '',
    component: CharacterListComponent
  },
  {
    path: ':id',
    component: CharacterDetailComponent,
    resolve: {
      character: ItemResolver
    }
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class CharacterRoutingModule { }
