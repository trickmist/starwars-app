import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/@shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { FilmRoutingModule, ItemResolver } from './film-routing.module';
import { FilmDetailComponent } from './components/film-detail/film-detail.component';
import { FilmListComponent } from './components/film-list/film-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FilmListComponent,
    FilmDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    FilmRoutingModule,
    SharedModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [
    ItemResolver
  ]
})
export class FilmModule { }
