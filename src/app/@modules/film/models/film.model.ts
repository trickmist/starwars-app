import { Injectable } from '@angular/core';
import { Adapter } from '../../../Library/adapter';
import { Resource as ResourceInterface } from '../../../Library/resource/resource';
import { Resource } from '../../../Library/resource/resource.model';
import { UserService } from '../../user/services/user.service';

export class Film extends Resource<Request> implements ResourceInterface<Film> {
  public id?: number;
  public title?: string;
  public episode_id?: number;
  public opening_crawl?: string;
  public director?: string;
  public producer?: string;
  public release_date?: string;
  public characters?: string[];
  public planets?: string[];
  public starships?: string[];
  public species?: string;
  public vehicles?: string;
  public url?: string;
  public created?: string;
  public edited?: string;

  public constructor(data?: {}) {
    super(data);
    this.init();
  }

  init(): void {
  }

}

@Injectable({
  providedIn: "root"
})
export class FilmAdapter implements Adapter<Film> {

  constructor(private userService: UserService) {
  }

  adapt(data: any): Film {
    const url = data.url.split('/');
    data.id = url[url.length - 2];
    data.favorite = this.userService.getFavorites().indexOf(data.id) >= 0;
    return new Film(data);
  }

  adaptArray(data: any): Film[] {
    data = data.results;
    return data.map((item: Film) => {
      return this.adapt(item);
    });
  }
}
