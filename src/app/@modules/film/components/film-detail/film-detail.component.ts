import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take, takeUntil, tap } from 'rxjs/operators';
import { Film } from '../../models/film.model';
import { Subject } from 'rxjs';
import { FilmService } from '../../services/film.service';
import { UserService } from '../../../user/services/user.service';
import { Character } from '../../../character/models/character.model';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.scss']
})
export class FilmDetailComponent implements OnInit, OnDestroy {

  ngUnsubscribe = new Subject();

  film: Film = new Film();
  characters?: Character[] = undefined;
  displayedColumns = ['character-id', 'character-name'];


  constructor(
    private activatedRoute: ActivatedRoute,
    breakpointObserver: BreakpointObserver,
    private router: Router,
    private filmService: FilmService,
    private userService: UserService
  ) {

    const layoutChanges = breakpointObserver.observe([
      '(max-width: 369.98px)',
      '(min-width: 370px) and (max-width: 599.98px)',
      Breakpoints.Small
    ]);
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.data.film) {
      this.film = this.activatedRoute.snapshot.data.film;
    }

    if (this.film) {
      this.filmService.getCharactersByFilm(this.film).pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe(((characters: Character[]) => {
        this.characters = characters;
      }));
    }
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
