import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Film } from '../../models/film.model';
import { FilmService } from '../../services/film.service';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent implements OnInit, OnDestroy {

  films?: Film[] = [];
  ngUnsubscribe = new Subject();
  page: number = 0;
  search: string = '';
  ordering: string = 'name';
  orderingOptions = [{
    label: 'Title (ascending)',
    ordering: 'title'
  }, {
    label: 'Title (descending)',
    ordering: '-title'
  }];
  itemColSize: number = 4;
  itemCardHeight: number = 300;
  filmsCount: number = 0;
  pageSize = 12;

  constructor(
    private activatedRoute: ActivatedRoute,
    private filmService: FilmService,
    private router: Router,
    breakpointObserver: BreakpointObserver
  ) {
    const layoutChanges = breakpointObserver.observe([
      '(max-width: 369.98px)',
      '(min-width: 370px) and (max-width: 599.98px)',
      Breakpoints.Small,
      Breakpoints.Medium
    ]);
    layoutChanges.subscribe((result: { matches: boolean, breakpoints: { [key: string]: boolean } }) => {
      const matchedBreakpoint = Object.values(result.breakpoints).indexOf(true);
      switch (matchedBreakpoint) {
        case 0:
          this.itemColSize = 1;
          break;
        case 1:
          this.itemColSize = 2;
          break;
        case 2:
          this.itemColSize = 2;
          break;
        default:
          this.itemColSize = 5;
      }

    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.pipe(
      takeUntil(this.ngUnsubscribe),
      tap(queryParams => {
        if (queryParams.page && Number.isInteger(+queryParams.page)) {
          this.page = queryParams.page - 1;
        }
        if (queryParams.ordering) {
          this.ordering = queryParams.ordering;
        }
        if (queryParams.search) {
          this.search = queryParams.search;
        } else {
          this.search = '';
        }
        this.loadItems();
      })).subscribe();
  }

  loadItems(): void {
    const params = {
      page: +this.page + 1,
      ordering: this.ordering,
      search: this.search.length > 0 ? this.search : null
    };
    this.filmService.all(params).pipe(
      takeUntil(this.ngUnsubscribe),
      tap(response => {
        this.filmsCount = response.count ? response.count : 0;
        this.films = response.results;
      })
    ).subscribe();
  }

  onPageChanged(event: PageEvent) {
    this.router.navigate(['/films/'], {
      queryParams: {
        page: event.pageIndex+1,
        ordering: this.ordering
      },
      queryParamsHandling: 'merge'
    });
  }

  onOrderingChanged() {
    this.router.navigate(['/films/'], {
      queryParams: {
        page: 1,
        ordering: this.ordering
      },
      queryParamsHandling: 'merge'
    });
  }

  onSearch() {
    this.router.navigate(['/films/'], {
      queryParams: {
        page: 1,
        search: this.search
      },
      queryParamsHandling: 'merge'
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
