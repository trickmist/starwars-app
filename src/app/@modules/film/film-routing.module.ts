import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot } from '@angular/router';
import { ResourceResolver } from 'src/app/Library/resource/resource.resolver';
import { GlobalService } from 'src/app/Library/services/global.service';
import { FilmModule } from './film.module';
import { FilmDetailComponent } from './components/film-detail/film-detail.component';
import { FilmListComponent } from './components/film-list/film-list.component';
import { Film } from './models/film.model';
import { FilmService } from './services/film.service';
import { Observable, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ItemResolver<T> implements Resolve<T> {
  constructor(
    private globalService: GlobalService,
    private service: FilmService,
    private httpClient: HttpClient
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Film> | Promise<Film> | any {
    return this.service.get(route.paramMap.get('id')).pipe(
      catchError(error => {
        this.globalService.is404.next(true);
        return of(error);
      }));
  }
}

const routes = [
  {
    path: '',
    component: FilmListComponent
  },
  {
    path: ':id',
    component: FilmDetailComponent,
    resolve: {
      film: ItemResolver
    }
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class FilmRoutingModule { }
