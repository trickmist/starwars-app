import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResourceService } from '../../../Library/resource/resource.service';
import { Film, FilmAdapter } from '../models/film.model';
import { combineLatest, Observable } from 'rxjs';
import { Character, CharacterAdapter } from '../../character/models/character.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: "root"
})
export class FilmService extends ResourceService<Film> {

  constructor(
    httpClient: HttpClient,
    adapter: FilmAdapter,
    private characterAdapter: CharacterAdapter
  ) {
    super('films', httpClient, adapter);
  }

  getCharactersByFilm(film: Film): Observable<Character[]> {
    const characterObservables: Observable<Character>[] = film.characters!.map(
      character => this.httpClient.get<Character>(character)
    );
    return combineLatest(characterObservables).pipe(
      map((response: Character[]) => {
        return this.characterAdapter.adaptArray({results: response});
      })
    );
  }

}
