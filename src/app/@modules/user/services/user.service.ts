import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: "root"
})
export class UserService {

  private favoritesSubject$: BehaviorSubject<number[]> = new BehaviorSubject<number[]>([]);
  favoritesObservable$: Observable<number[]> = this.favoritesSubject$.asObservable();

  constructor() {
    const favorites = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')!) : [];
    this.favoritesSubject$.next(favorites);
  }

  getFavorites(): number[] {
    return this.favoritesSubject$.value;
  }

  addToFavorites(id: number): void {
    const favorites = this.getFavorites();
    favorites.push(id);
    this.favoritesSubject$.next(favorites);
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }

  removeFromFavorites(id: number): void {
    const favorites = this.getFavorites();
    while (favorites.indexOf(id) >= 0) {
      favorites.splice(favorites.indexOf(id), 1);
    }
    this.favoritesSubject$.next(favorites);
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }

}
