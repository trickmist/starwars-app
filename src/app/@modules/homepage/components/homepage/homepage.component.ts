import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  vehicleColSize: number = 4;
  popularMakesColSize: number = 4;
  defaultImage = 'assets/homepage/gfx_hero_image_2560_fullHD.jpg';
  images = `assets/homepage/gfx_hero_image_1024.jpg 1024w,
            assets/homepage/gfx_hero_image_1440.jpg 1440w,
            assets/homepage/gfx_hero_image_1920.jpg 1920w`;

  constructor(
    private activatedRoute: ActivatedRoute,
    breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit(): void {}

}
