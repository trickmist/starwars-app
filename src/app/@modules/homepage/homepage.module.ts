import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/@shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { HomepageComponent } from './components/homepage/homepage.component';
import { HomepageRoutingModule } from './homepage-routing.module';


@NgModule({
  declarations: [
    HomepageComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    HomepageRoutingModule,
    SharedModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: []
})
export class HomepageModule { }
