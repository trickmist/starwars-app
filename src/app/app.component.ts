import { Component, OnInit } from '@angular/core';
import { ResolveStart, Router } from '@angular/router';
import { GlobalService } from './Library/services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'starwars-app';

  constructor(
    private router: Router,
    private globalService: GlobalService
  ) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof ResolveStart && this.globalService.is404.value) {
        this.globalService.is404.next(false);
      }
    });
  }
}
