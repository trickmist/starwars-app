import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../Library/services/global.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  public is404Subject: BehaviorSubject<boolean>;

  constructor(globalService: GlobalService) {
    this.is404Subject = globalService.is404;
  }

  ngOnInit(): void {
  }

}
