import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { ServerResponseService } from 'src/app/Library/server-response.service';
// import { LocaleService } from 'src/app/Library/services/locale.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent implements OnInit {

  constructor(
      // private srs: ServerResponseService,
      private router: Router,
      // private localeService: LocaleService
    ) {
    // srs.setNotFound();
  }

  ngOnInit(): void {
  }

}
